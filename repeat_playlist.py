class Song:
    def __init__(self, name):
        self.name = name
        self.next = None
    def next_song(self, song):
        self.next = song
    def is_repeating_playlist(self):
        """
        :returns: (bool) True if the playlist is repeating, False if not.
        """
        # == Add your code here! ==
        song_list = set()
        current_song = self.next
        while(current_song):
            if current_song.name in song_list:
                return True
            song_list.add(current_song.name)
            current_song = current_song.next
        return None

first = Song("Hello")
second = Song("Eye of the tiger")
first.next_song(second);
second.next_song(first);
print(first.is_repeating_playlist())
