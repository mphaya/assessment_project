import collections
Node = collections.namedtuple('Node', ['left', 'right', 'value'])

def contains(node, value):
    # == Add your code here! ==
    if node.value is None:
        return False
    else:
        if node.value == value:
            return True
        elif node.value > value:
            return node.left.value == value

        return node.right.value == value


n1 = Node(value=1, left=None, right=None)
n3 = Node(value=3, left=None, right=None)
n2 = Node(value=2, left=n1, right=n3)
print(contains(n2, 2))
